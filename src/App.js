import React, { useState } from "react";
import { Hero, Container, Heading, Button } from "react-bulma-components";
import Typist from 'react-typist';
import "./App.css";

import "react-bulma-components/dist/react-bulma-components.min.css";

// components
import About from "./components/About";
import Contact from "./components/Contact";
import Project from "./components/Project";
import HeroFooter from "./components/HeroFooter";
import EducBackground from "./components/EducBackground";
import WorkingExperience from "./components/WorkingExperience";
import WorkingExperiencecwd from "./components/WorkingExperiencecwd";
import WorkingExperiencewilcon  from "./components/WorkingExperiencewilcon";
import ZuittBootcamp from "./components/ZuittBootcamp";
import Skills from "./components/Skills";
import FooterContent from "./components/Footer";

function App() {
  //hooks
  const [whichComponentToShow, setComponentToShow] = useState("landing");

  const landingClickHandler = () => {
    setComponentToShow("landing");
  };

  const aboutClickHandler = () => {
    setComponentToShow("about");
  };

  const contactClickHandler = () => {
    setComponentToShow("contact");
  };

  const projectsClickHandler = () => {
    setComponentToShow("projects");
  };

  const educBackgroundClickHandler = () => {
    setComponentToShow("educBackground");
  };

  const workingExperienceClickHandler = () => {
    setComponentToShow("workingExperience");
  };

  const workingExperienceCwdClickHandler = () => {
    setComponentToShow("workingExperiencecwd"); 
  }

  const workingExperienceWilconClickHandler = () => {
    setComponentToShow("workingExperiencewilcon"); 
  }

  const zuittBootcampClickHandler = () => {
    setComponentToShow("zuittBootcamp");
  };

  const skillsClickHandler = () => {
    setComponentToShow("skills");
  };

  //ABOUT
  if (whichComponentToShow === "about") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
          
        />
        
        <Hero.Body>
          <About educBackground={educBackgroundClickHandler} />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  //CONTACT
  else if (whichComponentToShow === "contact") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <Contact />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  //PROJECT
  else if (whichComponentToShow === "projects") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <Project />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  //EDUCATIONAL BACKGROUND
  else if (whichComponentToShow === "educBackground") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <EducBackground
            workingExperience={workingExperienceClickHandler}
            about={aboutClickHandler}
          />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  //WORKING EXPERIENCE AT LUCKY CIRCLE CORPORATION
  else if (whichComponentToShow === "workingExperience") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <WorkingExperience
            educBackground={educBackgroundClickHandler}
            workingExperienceCwd={workingExperienceCwdClickHandler}
          />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  //WORKING EXPERIENCE AT CLOUDWALK DIGITAL

  else if (whichComponentToShow === "workingExperiencecwd") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <WorkingExperiencecwd
            workingExperience={workingExperienceClickHandler}
            workingExperienceWilcon={workingExperienceWilconClickHandler}
          />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }


    //WORKING EXPERIENCE AT WILCON DEPOT INC

    else if (whichComponentToShow === "workingExperiencewilcon") {
      return (
        <Hero size="fullheight" color="dark" gradient>
          <HeroFooter
            about={aboutClickHandler}
            projects={projectsClickHandler}
            contact={contactClickHandler}
            landing={landingClickHandler}
          />
          <Hero.Body>
            <WorkingExperiencewilcon
              workingExperienceCwd={workingExperienceCwdClickHandler}
              zuittBootcamp={zuittBootcampClickHandler}
            />
          </Hero.Body>
        <FooterContent />
        </Hero>
      );
    }



  //ZUITT BOOTCAMP
  else if (whichComponentToShow === "zuittBootcamp") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <ZuittBootcamp
            workingExperienceWilcon={workingExperienceWilconClickHandler}
            skills={skillsClickHandler}
          />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  // Skills
  else if (whichComponentToShow === "skills") {
    return (
      <Hero size="fullheight" color="dark" gradient>
        <HeroFooter
          about={aboutClickHandler}
          projects={projectsClickHandler}
          contact={contactClickHandler}
          landing={landingClickHandler}
        />
        <Hero.Body>
          <Skills zuittBootcamp={zuittBootcampClickHandler} />
        </Hero.Body>
      <FooterContent />
      </Hero>
    );
  }

  //LANDING PAGE
  else if (whichComponentToShow === "landing") {
    return (
      <Hero size="fullheight" id="landing-page" style={{backgroundImage : `url(images/bg.jpg)`}}>
        <Hero.Body>
          <Container className="has-text-centered">
            <Heading size={1}>
              <h1 className="heading-text has-text-left">
                <span className="span-gold">-</span>Hi There!
              </h1>
              <h1 className="heading-text has-text-left">
                I'm <span className="span-gold">
                John Lester
                </span>
              </h1>
              <h1 className="heading-text has-text-left">Sabandal</h1>
            </Heading>
            <Heading subtitle size={5} className="has-text-left" id="margin">
              <span>
                <Typist>
                  Web Designer | Web Developer 
                </Typist>
              </span>
            </Heading>
            <Heading className="has-text-left" id="margin">
              <Button id="button-gold" onClick={aboutClickHandler}>
                About me
              </Button>
            </Heading>
          </Container>
        </Hero.Body>
      </Hero>
    );
  }
  return null;
}

export default App;
