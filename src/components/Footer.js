import React, {useState} from "react";
import {
	Hero,
 	Footer, 
 	Container,
 	Content
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const FooterContent = props => {

	return (
		    <Hero>	
		      <Hero.Footer>
		      <Fade big>
		        <Footer id="custom-nav" className="custom-footer">
		          <Container>
		            <Content   className="has-text-centered">
		          {/*
		              <p>
		                <span id="about-span">
							<i class="fab fa-facebook-square fa-lg" id="about-icon"></i>
						</span>
						<span id="about-span">
							<i class="fab fa-linkedin fa-lg" id="about-icon"></i>
						</span>
		              </p>
		          */}
		          	<h3>
			          	<span className="contextcolor2">
								JOHN LESTER SABANDAL
						</span>
		          	</h3>
					<span className="contextcolor2">
						+63 921 276 5022
					</span>
					<p className="contextcolor2">             	  
		            	48-A Sampaguit Street Sitio Incinerator Brgy. Bahay Toro Project 8 Q.C
		            	<br/>
		            	Quezon City Philippines 1106
		            </p>
		            <br/>
		            <p>
		            	<h3>
			            	<span className="contextcolor2">
			            		LET'S CONNECT!
			            	</span>
		            	</h3>
			        <span id="devicon-span">
			            <a
								href="https://github.com/jlretzel07"
								target="_blank"
						>
								<i class="fab fa-github custom-devicon3"></i>
						</a>
					</span>
					
					<span id="devicon-span">
						<a
								href="https://gitlab.com/jl.sabandal"
								target="_blank"
						>
								<i class="fab fa-gitlab custom-devicon3"></i>
						</a>
					</span>
					<span id="devicon-span">
						<a
							href="https://www.linkedin.com/in/john-lester-sabandal-331b52105/"
							target="_blank"
						>
							<i class="fab fa-linkedin  custom-devicon3"></i>
						</a>
					</span>
					<span id="devicon-span">
						<a
							href="https://www.facebook.com/jlretzel07"
							target="_blank"
						>
								<i class="fab fa-facebook custom-devicon3"></i>
						</a>
					</span>
		            </p>
		            <br/>
		              <p className="contextcolor2">             	  
		              	 © 2019 All Rights Reserved By John Lester Sabandal 
		              </p>
		            </Content>
		          </Container>
		        </Footer>
		        </Fade>
		      </Hero.Footer>
		    </Hero>
		);

}


export default FooterContent;



