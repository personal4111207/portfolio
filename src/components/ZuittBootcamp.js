import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Box,
	Tile,
	Table,
	Button,
	Image
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const ZuittBootcamp = props => {
	// console.log(props);
	const workingExperienceWilconClickHandler = () => {
		props.workingExperienceWilcon();
	};

	const sksillsClickHandler = () => {
		props.skills();
	}

	return (
		<Container className="has-text-centered margin">
			<Columns>
				<Columns.Column id="contact-form" size={6}>
				<Fade big>
					<h1 id="title" className="is-uppercase">
						Training - Zuitt Coding Bootcamp
					</h1>
					<hr className="hr" />
					<Columns.Column id="contact-info-educ">
						<p className="contextcolor line-height">
							A web development bootcamp focusing on both frontend
							technologies such as HTML,CSS and Bootsrap; and
							backend technologies such as Javascript, PHP, MySQL,
							Laravel, MongoDB, Express and NodeJS
						</p>
					</Columns.Column>
					<Image src="images/3.jpg" id="image" />
				</Fade>	
				</Columns.Column>
				<Columns.Column
					size={6}
					className="has-text-left"
					id="contact-form"
				>
				<Fade big>
					<h1 id="title" className="has-text-centered is-uppercase">
						technologies
					</h1>
					<hr className="hr" />

					<Columns.Column>
						<Columns>
							<Columns.Column
								id="zuitt-info-educ"
								size={6}
								className="has-text-centered"
							>
								<h1 className="contextcolortitle is-uppercase">
									Front End
								</h1>
								<hr className="hr2" />
								<span id="devicon-span">
									<i class="devicon-html5-plain-wordmark custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-css3-plain-wordmark custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-jquery-plain-wordmark custom-devicon"></i>
								</span>
							</Columns.Column>

							<Columns.Column
								id="zuitt-info-educ"
								size={6}
								className="has-text-centered"
							>
								<h1 className="contextcolortitle is-uppercase">
									Scripting Language
								</h1>
								<hr className="hr2" />
								<span id="devicon-span">
									<i class="devicon-php-plain custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-javascript-plain custom-devicon"></i>
								</span>
							</Columns.Column>
						</Columns>
					</Columns.Column>

					<Columns.Column>
						<Columns>
							<Columns.Column
								id="zuitt-info-educ"
								size={6}
								className="has-text-centered"
							>
								<h1 className="contextcolortitle is-uppercase">
									Frameworks
								</h1>
								<hr className="hr2" />
								<Columns.Column>
									<Columns>
										<Columns.Column>
											<span id="devicon-span">
												<i class="devicon-laravel-plain-wordmark custom-devicon"></i>
											</span>
											<span id="devicon-span">
												<i class="devicon-react-original-wordmark custom-devicon"></i>
											</span>
											<span id="devicon-span">
												<i class="devicon-express-original-wordmark custom-devicon"></i>
											</span>
											<span id="devicon-span">
												<i class="devicon-nodejs-plain custom-devicon"></i>
											</span>
										</Columns.Column>
									</Columns>
									<Columns>
										<Columns.Column>
											<span id="devicon-span">
											<i class="devicon-vuejs-plain-wordmark custom-devicon"></i>
											</span>
										</Columns.Column>
									</Columns>
								</Columns.Column>
							</Columns.Column>

							<Columns.Column
								id="zuitt-info-educ"
								size={6}
								className="has-text-centered"
							>
								<h1 className="contextcolortitle is-uppercase">
									CSS Frameworks
								</h1>
								<hr className="hr2" />
								<span id="devicon-span">
									<i class="devicon-bootstrap-plain-wordmark custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-sass-original custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="custom-devicon">Buefy</i>
								</span>
								

							</Columns.Column>
						</Columns>
					</Columns.Column>

					<Columns.Column id="zuitt-info-educ2">
						<Columns>
							<Columns.Column
								size={6}
								className="has-text-centered"
							>
								<h1 className="contextcolortitle is-uppercase">
									Database
								</h1>
								<hr className="hr2" />
								<span id="devicon-span">
									<i class="devicon-mysql-plain-wordmark custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-mongodb-plain-wordmark custom-devicon"></i>
								</span>
							</Columns.Column>

							<Columns.Column
								size={6}
								className="has-text-centered"
							>
								<h1 className="contextcolortitle is-uppercase">
									Others
								</h1>
								<hr className="hr2" />
								<span id="devicon-span">
									<i class="devicon-github-plain-wordmark custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-gitlab-plain-wordmark custom-devicon"></i>
								</span>
								<span id="devicon-span">
									<i class="devicon-heroku-original-wordmark custom-devicon"></i>
								</span>
							</Columns.Column>
						</Columns>
					</Columns.Column>

					<Columns>
						<Columns.Column size={6}>
							<span className="contextcolort">Follow Me :</span>
							<span id="about-span">
								<a
										href="https://www.facebook.com/jlretzel07"
										target="_blank"
									>
										<i
											class="fab fa-facebook-square fa-lg"
											id="about-icon"
										></i>
									</a>
								</span>
								<span id="about-span">
									<a
										href="https://www.linkedin.com/in/john-lester-sabandal-331b52105/"
										target="_blank"
									>
									<i
										class="fab fa-linkedin fa-lg"
										id="about-icon"
									></i>
									</a>
							</span>
						</Columns.Column>
						<Columns.Column size={6} className="has-text-right">
							<Button
								className="btn-next is-uppercase"
								onClick={workingExperienceWilconClickHandler}
							>
								back
							</Button>

							<Button
								className="btn-next is-uppercase"
								onClick={sksillsClickHandler}
							>
								Next
							</Button>
						</Columns.Column>
					</Columns>
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default ZuittBootcamp;
