import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Box,
	Tile,
	Table,
	Button,
	Image
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const About = props => {
	// console.log(props.educBackground);

	const educBackgroundClickHandler = () => {
		props.educBackground();
	};

	return (
		<Container className="has-text-centered margin">
			<Columns>
				<Columns.Column id="contact-form" size={6}>
					<Fade big>
					<h1 id="title">ABOUT ME</h1>
					<hr className="hr" />
					<Image src="images/me.jpeg" id="image" />
					</Fade>	
				</Columns.Column>

				<Columns.Column
					size={6}
					className="has-text-left"
					id="contact-form"
				>
				<Fade big>
					<h4>
						<span id="contact-span">
							<i class="fas fa-user-alt" id="contact-icon"></i>
						</span>
						<span className="span-gold" id="title">
							I'm John Lester Sabandal
						</span>
					</h4>
					<br />
					<p id="title2">I'm a Full Stack Web Developer</p>

					<p className="contextcolor margin-buttoma line-height">
						Thanks for visiting my website. If you are interested in
						finding more about me, my interests, projects or skills,
						please browse around.
					</p>

					<Columns.Column id="contact-info-educ">
						<Columns>
							<Columns.Column size={6}>
								<p className="contextcolor">
									<b>Name&nbsp;:</b> &nbsp; John Lester Sabandal
								</p>
							</Columns.Column>
							<Columns.Column size={6}>
								<p className="contextcolor"><b>Age&nbsp;:</b>&nbsp; 28</p>
							</Columns.Column>
						</Columns>
						<Columns>
							<Columns.Column size={6}>
								<p className="contextcolor">
								<b>Contact No&nbsp;:</b> &nbsp; +63 921-276-5022
								</p>
							</Columns.Column>
							<Columns.Column size={6}>
								<p className="contextcolor">
									<b>Nationality&nbsp;:</b>&nbsp; Filipino
								</p>
							</Columns.Column>
						</Columns>
						{/* <Columns>
							<Columns.Column>
								<p className="contextcolor">
									<b>Address&nbsp;:</b>&nbsp; 48-A Sampaguit Street Sitio Incinerator Brgy. Bahay Toro Project 8 Q.C
								</p>
							</Columns.Column>
						</Columns> */}
					</Columns.Column>
					<Columns.Column>
						<Columns>
							<Columns.Column size={6}>
								<span className="contextcolort">
									Follow Me :
								</span>
								<span id="about-span">
									<a
										href="https://www.facebook.com/jlretzel07"
										target="_blank"
									>
										<i
											class="fab fa-facebook-square fa-lg"
											id="about-icon"
										></i>
									</a>
								</span>
								<span id="about-span">
									<a
										href="https://www.linkedin.com/in/john-lester-sabandal-331b52105/"
										target="_blank"
									>
									<i
										class="fab fa-linkedin fa-lg"
										id="about-icon"
									></i>
									</a>
								</span>
							</Columns.Column>
							<Columns.Column size={6} className="has-text-right">
								<Columns>
									<Columns.Column>
										<a
										href="https://drive.google.com/drive/folders/1-d3tRKvW-FFzzoG92B0kpwbEvl8IPzZn?usp=sharing"
										target="_blank"
										>
											<Button className="btn-next is-uppercase" fullwidth>
												download cv
											</Button>
										</a>
									</Columns.Column>
									<Columns.Column>
										<Button
											className="btn-next is-uppercase"
											onClick={educBackgroundClickHandler}											
										>
											Next
										</Button>
									</Columns.Column>
								</Columns>
							</Columns.Column>
						</Columns>
					</Columns.Column>
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default About;
