import React, { useState } from "react";
import { Hero, Tabs, Navbar } from "react-bulma-components";
const HeroFooter = props => {
	const [open, setOpen] = useState(false);	
	// console.log(props);
	//hooks
	const [visible, setVisible] = useState(false);
	const [about, setAbout] = useState(false);
	const [projects, setProjects] = useState(false);
	const [contact, setContact] = useState(false);

	const aboutClickHandler = () => {
		// console.log("you clicked about");
		setVisible(true);
		setAbout(true);
		setProjects(false);
		setContact(false);
		props.about();
	};

	const projectsClickHandler = () => {
		// console.log("you clicked Projects");
		setVisible(true);
		setAbout(false);
		setProjects(true);
		setContact(false);
		props.projects();
	};

	const contactClickHandler = () => {
		// console.log("you clicked contact");
		setVisible(true);
		setAbout(false);
		setProjects(false);
		setContact(true);
		props.contact();
	};

	const landingClickHandler = () =>{
		props.landing();
	}





	return (
		<Navbar id="custom-nav" active={open} className="is-fixed-top">
		 	 <Navbar.Brand>
	          <Navbar.Item onClick={landingClickHandler}>
	            <span className="span-gold-title is-uppercase">jhnlstrsbndl</span>
	          </Navbar.Item>
				<Navbar.Burger active={open} onClick={() => setOpen(!open)} />
        	</Navbar.Brand>

        	<Navbar.Menu>
        		<Navbar.Container position="end">
						<Navbar.Item

							active={visible && about ? true : false}
							onClick={aboutClickHandler}
							className="nav-text is-uppercase is-active"
						>
							about me
						</Navbar.Item>
						<Navbar.Item
							active={visible && projects ? true : false}
							onClick={projectsClickHandler}
							className="nav-text is-uppercase"
						>
							project
						</Navbar.Item>
						<Navbar.Item
							active={visible && contact ? true : false}
							onClick={contactClickHandler}
							className="nav-text is-uppercase"
						>
							contact
						</Navbar.Item>
        		</Navbar.Container>
        	</Navbar.Menu>
		</Navbar>
	);
};

export default HeroFooter;
