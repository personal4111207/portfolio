import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Progress,
	Button,
	Media,
	Image
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const Project = () => {
	return (
		<Container className="has-text-centered margin">
			<Columns>
				<Columns.Column id="contact-form">
				<Fade big>
					<h1>
						<span className="span-gold is-capitalized">
							lets' see my work
						</span>
					</h1>
					<h1 id="title" className="is-uppercase">
						my portfolio
					</h1>
					<hr className="hr" />

					<Columns>
						<Columns.Column size={4}>
							<Card id="contact-info-educ1">
							<div class="container1">
								<Card.Header className="cardheader">
									<Card.Header.Title
										className="is-uppercase"
										id="card-border"
									>
										<span className="span-gold">
											Static Website
										</span>
									</Card.Header.Title>
								</Card.Header>
									<Card.Content className="cardcontent cardheader">
										<Media>
											<Media.Item
												renderAs="figure"
												position="centered"
											>
												<a
													href="https://jl.sabandal.gitlab.io/capstoneproject1/ "
													target="_blank"
													className="afont"
												>
													<Image
														alt="100x100"
														src="images/c1.png"
													/>
													<br/>
													Username : N/A
													<br/>
													password : N/A
												</a>
											</Media.Item>
										</Media>
									</Card.Content>
									<div class="overlay overlayleft">
										<div class="text">
											
											E-Commerce Design
										</div>
										<div class="text3">
											<span color="dark" className="iconimg">
												<i class="devicon-html5-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-css3-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-jquery-plain-wordmark custom-devicon2"></i>			
											</span>
											<span color="dark" className="iconimg">											
												<i class="devicon-bootstrap-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-javascript-plain custom-devicon2"></i>
											</span>
										</div>
										<div class="icon">
											<span color="dark" className="">
												<i
													class="fas fa-image fa-2x"
													id="contact-icon"
												></i>
											</span>
										</div>
									</div>	
							</div>
								<small>
								<a
									href="https://jl.sabandal.gitlab.io/capstoneproject1/ "
									target="_blank"
								>
									<Button id="button-gold2">
						                View Page
						            </Button>
						        </a>    
								</small>
							</Card>
						</Columns.Column>

						<Columns.Column size={4}>
							<Card id="contact-info-educ1">
							<div class="container1">
								<Card.Header className="cardheader">
									<Card.Header.Title
										className="is-uppercase"
										id="card-border"
									>
										<span className="span-gold">
											Laravel - Asset Management
										</span>
									</Card.Header.Title>
								</Card.Header>
									<Card.Content className="cardcontent cardheader">
										<Media>
											<Media.Item
												renderAs="figure"
												position="centered"
											>
												<a
													href="https://jl.sabandal.gitlab.io/capstoneproject1/ "
													target="_blank"
													className="afont"
												>
													<Image
														alt="100x100"
														src="images/c2.png"
													/>													
													<br/>
													Username : admin@mail.com
													<br/>
													password : password
												
												</a>
											</Media.Item>
										</Media>
									</Card.Content>
									<div class="overlay overlayleft">
										<div class="text">
											Asset Management
										</div>
										<div class="text3">
											<span color="dark" className="iconimg">
												<i class="devicon-html5-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-css3-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">											
												<i class="devicon-bootstrap-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-php-plain custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-laravel-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-mysql-plain-wordmark custom-devicon2"></i>		
											</span>
										</div>
										<div class="icon">
											<span color="dark" className="">
												<i
													class="fas fa-image fa-2x"
													id="contact-icon"
												></i>
											</span>
										</div>
									</div>	
							</div>
								<small>
								<a
									href="http://laravel-ecom.herokuapp.com/products"
									target="_blank"
								>
									<Button id="button-gold2">
						                View Page
						            </Button>
						        </a>    
								</small>
							</Card>
						</Columns.Column>

						<Columns.Column size={4}>
							<Card id="contact-info-educ1">
							<div class="container1">
								<Card.Header className="cardheader">
									<Card.Header.Title
										className="is-uppercase"
										id="card-border"
									>
										<span className="span-gold">
											MERN - Booking system
										</span>
									</Card.Header.Title>
								</Card.Header>
									<Card.Content className="cardcontent cardheader">
										<Media>
											<Media.Item
												renderAs="figure"
												position="centered"
											>
												<a
													href="https://capstone3-client.herokuapp.com"
													target="_blank"
													className="afont"
												>
													<Image
														alt="100x100"
														src="images/c3.png"

													/>
													<br/>
													Username : admin@yahoo.com
													<br/>
													password : password
												</a>
											</Media.Item>
										</Media>
									</Card.Content>
									<div class="overlay overlayleft">
										<div class="text">
											Bookign System
										</div>
										<div class="text3">
											<span color="dark" className="iconimg">
												<i class="devicon-html5-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-css3-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">											
												<i class="devicon-react-original-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-nodejs-plain custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-express-original-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-mongodb-plain-wordmark custom-devicon2"></i>		
											</span>
										</div>
										<div class="icon">
											<span color="dark" className="">
												<i
													class="fas fa-image fa-2x"
													id="contact-icon"
												></i>
											</span>
										</div>
									</div>	
							</div>
								<small>
								<a
									href="https://capstone3-client.herokuapp.com"
									target="_blank"
								>
									<Button id="button-gold2">
						                View Page
						            </Button>
						        </a>    
								</small>
							</Card>
						</Columns.Column>
					</Columns>

					<Columns>
						<Columns.Column size={4}>
							<Card id="contact-info-educ1">
							<div class="container1">
								<Card.Header className="cardheader">
									<Card.Header.Title
										className="is-uppercase"
										id="card-border"
									>
										<span className="span-gold">
											Laravel - To Do List
										</span>
									</Card.Header.Title>
								</Card.Header>
									<Card.Content className="cardcontent cardheader">
										<Media>
											<Media.Item
												renderAs="figure"
												position="centered"
											>
												<a
													href="https://jl.sabandal.gitlab.io/capstoneproject1/ "
													target="_blank"
													className="afont"
												>
													<Image
														alt="100x100"
														src="images/todolist.JPG"
													/>													
												
												</a>
											</Media.Item>
										</Media>
									</Card.Content>
									<div class="overlay overlayleft">
										<div class="text">
											To Do List
										</div>
										<div class="text3">
											<span color="dark" className="iconimg">
												<i class="devicon-html5-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-css3-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">											
												<i class="devicon-bootstrap-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-php-plain custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-laravel-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-mysql-plain-wordmark custom-devicon2"></i>		
											</span>
										</div>
										<div class="icon">
											<span color="dark" className="">
												<i
													class="fas fa-image fa-2x"
													id="contact-icon"
												></i>
											</span>
										</div>
									</div>	
							</div>
								<small>
								<a
									href="http://web-app-todo-list.herokuapp.com/"
									target="_blank"
								>
									<Button id="button-gold2">
						                View Page
						            </Button>
						        </a>    
								</small>
							</Card>
						</Columns.Column>

						<Columns.Column size={4}>
							<Card id="contact-info-educ1">
							<div class="container1">
								<Card.Header className="cardheader">
									<Card.Header.Title
										className="is-uppercase"
										id="card-border"
									>
										<span className="span-gold">
											Laravel - CRUD
										</span>
									</Card.Header.Title>
								</Card.Header>
									<Card.Content className="cardcontent cardheader">
										<Media>
											<Media.Item
												renderAs="figure"
												position="centered"
											>
												<a
													href="https://jl.sabandal.gitlab.io/capstoneproject1/ "
													target="_blank"
													className="afont"
												>
													<Image
														alt="100x100"
														src="images/laravelcrud.JPG"
													/>													
												
												</a>
											</Media.Item>
										</Media>
									</Card.Content>
									<div class="overlay overlayleft">
										<div class="text">
											Laravel - CRUD
										</div>
										<div class="text3">
											<span color="dark" className="iconimg">
												<i class="devicon-html5-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-css3-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">											
												<i class="devicon-bootstrap-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-php-plain custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-laravel-plain-wordmark custom-devicon2"></i>
											</span>
											<span color="dark" className="iconimg">
												<i class="devicon-mysql-plain-wordmark custom-devicon2"></i>		
											</span>
										</div>
										<div class="icon">
											<span color="dark" className="">
												<i
													class="fas fa-image fa-2x"
													id="contact-icon"
												></i>
											</span>
										</div>
									</div>	
							</div>
								<small>
								<a
									href="http://basic-laravel-crud.herokuapp.com/clients"
									target="_blank"
								>
									<Button id="button-gold2">
						                View Page
						            </Button>
						        </a>    
								</small>
							</Card>
						</Columns.Column>

					</Columns>
					
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default Project;
