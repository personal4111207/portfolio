import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Box,
	Tile,
	Table,
	Button,
	Image
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const EducBackground = props => {
	// console.log(props);

	const workExperinceClickHandler = () => {
		props.workingExperience();
	};

	const aboutClickHandler = () => {
		props.about();
	};

	return (
		<Container className="has-text-centered margin">
			<Columns>
				<Columns.Column id="contact-form" size={6}>
					<h1 id="title">ABOUT ME</h1>
					<hr className="hr" />
					<Image src="images/me.jpeg" id="image" />
				</Columns.Column>

				<Columns.Column
					size={6}
					className="has-text-left"
					id="contact-form"
				>
					<Fade big>
					<h1 id="title" className="has-text-centered is-uppercase">
						EDUCATIONAL BACKGROUND
					</h1>
					<hr className="hr" />

					<span id="span-educ" className="contextcolort">
						2012 - 2016
					</span>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							<span id="about-span">
								<i class="fa fa-university fa-lg span-gold"></i>
							</span>
							tertiary
						</h1>
						<hr className="hr2" />
						<p className="is-capital contextcolor">
							Quezon City Polytechnic University
						</p>
						<p className="is-capital contextcolor">
							Bachelor of Science in Information Technology
						</p>
					</Columns.Column>

					<span id="span-educ" className="contextcolort">
						2006 - 2010
					</span>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							<span id="about-span">
								<i class="fa fa-school fa-lg span-gold"></i>
							</span>
							secondary
						</h1>
						<hr className="hr2" />
						<p className="is-capital contextcolor">
							Sergio Osmena Sr. High School
						</p>
						<p className="is-capital contextcolor">
							Cang-Alwang National High School
						</p>
					</Columns.Column>

					<span id="span-educ" className="contextcolort">
						2000 - 2006
					</span>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							<span id="about-span">
								<i class="fa fa-school fa-lg span-gold"></i>
							</span>
							primary
						</h1>
						<hr className="hr2" />
						<p className="is-capital contextcolor">
							Toro Hills Elementary School
						</p>
					</Columns.Column>

					<Columns>
						<Columns.Column size={6}>
							<span className="contextcolort">Follow Me :</span>
							<span id="about-span">
								<a
										href="https://www.facebook.com/jlretzel07"
										target="_blank"
									>
										<i
											class="fab fa-facebook-square fa-lg"
											id="about-icon"
										></i>
									</a>
								</span>
								<span id="about-span">
									<a
										href="https://www.linkedin.com/in/john-lester-sabandal-331b52105/"
										target="_blank"
									>
									<i
										class="fab fa-linkedin fa-lg"
										id="about-icon"
									></i>
									</a>
							</span>
						</Columns.Column>
						<Columns.Column size={6} className="has-text-right">
							<Button
								className="btn-next is-uppercase"
								onClick={aboutClickHandler}
							>
								Back
							</Button>

							<Button
								className="btn-next is-uppercase"
								onClick={workExperinceClickHandler}
							>
								Next
							</Button>
						</Columns.Column>
					</Columns>
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default EducBackground;
