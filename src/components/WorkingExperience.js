import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Box,
	Tile,
	Table,
	Button,
	Image
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const WorrkingExperience = props => {
	// console.log(props);
	const educBackgroundClickHandler = () => {
		props.educBackground();
	};

	const workingExperienceCwdClickHandler = () => {
		props.workingExperienceCwd();
	};

	return (
		<Container className="has-text-centered margin">
			<Columns>
				<Columns.Column id="contact-form" size={6}>
					<h1 id="title">ABOUT ME</h1>
					<hr className="hr" />
					<Image src="images/lester.jpg" id="image" />
				</Columns.Column>

				<Columns.Column
					size={6}
					className="has-text-left"
					id="contact-form"
				>
				<Fade big>
					<h1 id="title" className="has-text-centered is-uppercase">
						Working Experience
					</h1>
					<hr className="hr" />

					<span id="span-educ" className="contextcolort is-uppercase">
						June 2016 - July 2019
					</span>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							<span id="about-span">
								<i class="fa fa-briefcase fa-lg span-gold"></i>
							</span>
							MIS/IT Assistant
						</h1>
						<hr className="hr2" />
						<p className="is-capital contextcolortitle is-uppercase">
							Lucky Circle Corporation
						</p>
						<p className="is-capital contextcolor">
							20th Flr. West Tower Philippine Stock Exchange
							Center, Brgy. San Antonio, Pasig City
						</p>
					</Columns.Column>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							Job Responsibilities
						</h1>
						<hr className="hr2" />
						<ul style={{listStyleType : "circle", marginLeft : "15px"}}>
							<li className="is-capital contextcolor">
								SAP Business One Administrator{" "}
							</li>
							<li className="is-capital contextcolor">
								Mysql Server Administrator
							</li>
							<li className="is-capital contextcolor">
								Creating SAP Crystal Report
							</li>
							<li className="is-capital contextcolor">
								SAP Functional and Technical Support
							</li>
						</ul>
					</Columns.Column>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							SAP B1 Trainings
						</h1>
						<hr className="hr2" />
						<p className="is-capital contextcolor">
							• Crystal Report • Purchase Order • A/P Invoice •
							A/R invoice • Fixed Asset • Incoming Payments •
							Outgoing Payments
						</p>
					</Columns.Column>

					<Columns>
						<Columns.Column size={6}>
							<span className="contextcolort">Follow Me :</span>
							<span id="about-span">
								<a
										href="https://www.facebook.com/jlretzel07"
										target="_blank"
									>
										<i
											class="fab fa-facebook-square fa-lg"
											id="about-icon"
										></i>
									</a>
								</span>
								<span id="about-span">
									<a
										href="https://www.linkedin.com/in/john-lester-sabandal-331b52105/"
										target="_blank"
									>
									<i
										class="fab fa-linkedin fa-lg"
										id="about-icon"
									></i>
									</a>
							</span>
						</Columns.Column>
						<Columns.Column size={6} className="has-text-right">
							<Button
								className="btn-next is-uppercase"
								onClick={educBackgroundClickHandler}
							>
								back
							</Button>

							<Button
								className="btn-next is-uppercase"
								onClick={workingExperienceCwdClickHandler}
							>
								Next
							</Button>
						</Columns.Column>
					</Columns>
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default WorrkingExperience;
