import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';


const Contact = () => {
	return (
		<Container className="has-text-centered margin" id="contact-container">
			<Columns>
				<Columns.Column id="contact-form" size={8}>
				<Fade big>
					<h1>
						<span className="span-gold">Get in Touch</span>
					</h1>
					<h1 id="title">CONTACT ME</h1>
					<hr className="hr" />
					<form>
						<div className="field has-text-left">
							<div className="control">
								<label
									className="label has-text-light"
									for="name"
								>
									Name
								</label>
								<input
									id="input"
									className="input"
									type="text"
									placeholder="Name"
								/>
							</div>
						</div>
						<div className="field has-text-left">
							<div className="control">
								<label
									className="label has-text-light"
									for="name"
								>
									Email
								</label>
								<input
									id="input"
									className="input"
									type="email"
									placeholder="Email"
								/>
							</div>
						</div>
						<div className="field has-text-left">
							<div className="control">
								<label
									className="label has-text-light"
									for="name"
								>
									Message
								</label>
								<textarea
									id="input"
									className="textarea"
									placeholder="Input Message here"
								></textarea>
							</div>
						</div>
						<button
							className="button is-primary is-fullwidth"
							id="button-gold"
						>
							Send Message
						</button>
					</form>
					</Fade>
				</Columns.Column>
				<Columns.Column
					size={4}
					className="has-text-left"
					id="contact-form"
				>
				<Fade big>
					<Card id="contact-info-educ">
						<Card.Content>
							<Content>
								<h4>
									<span id="contact-span">
										<i
											class="fas fa-location-arrow"
											id="contact-icon"
										></i>
									</span>
									<span>LOCATION</span>
								</h4>
								<hr className="hr" />
								<p className="contextcolor">
									48-A Sampgauita St. Sitio Incinerator Brgy.
									Bahay Toro Proj.8 Quezon City
								</p>
							</Content>
						</Card.Content>
					</Card>
					<Card id="contact-info-educ">
						<Card.Content>
							<Content>
								<h4>
									<span id="contact-span">
										<i
											class="fas fa-envelope"
											id="contact-icon"
										></i>
									</span>
									<span>Email</span>
								</h4>
								<hr className="hr" />
								<p className="contextcolor">
									Johnlestersabandal@gmail.com
								</p>
							</Content>
						</Card.Content>
					</Card>
					<Card id="contact-info-educ">
						<Card.Content>
							<Content>
								<h4>
									<span id="contact-span">
										<i
											class="fas fa-phone"
											id="contact-icon"
										></i>
									</span>
									<span>Call me</span>
								</h4>
								<hr className="hr" />
								<p className="contextcolor">+63 921-276-5022</p>
							</Content>
						</Card.Content>
					</Card>
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default Contact;
