import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Box,
	Tile,
	Table,
	Button,
	Image
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const WorkingExperiencecwd = props => {
	// console.log(props);
	const workingExperienceClickHandler = () => {
		props.workingExperience();
	};

	const workingExperienceWilconClickHandler = () => {
		props.workingExperienceWilcon();
	};

	return (
		<Container className="has-text-centered margin">
			<Columns>
				<Columns.Column id="contact-form" size={6}>
					<h1 id="title">ABOUT ME</h1>
					<hr className="hr" />
					<Image src="images/cwd.png" id="image" />
				</Columns.Column>

				<Columns.Column
					size={6}
					className="has-text-left"
					id="contact-form"
				>
				<Fade big>
					<h1 id="title" className="has-text-centered is-uppercase">
						Working Experience
					</h1>
					<hr className="hr" />

					<span id="span-educ" className="contextcolort is-uppercase">
						March 2020 - January 2021
					</span>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							<span id="about-span">
								<i class="fa fa-briefcase fa-lg span-gold"></i>
							</span>
							Junior Software Engineer
						</h1>
						<hr className="hr2" />
						<p className="is-capital contextcolortitle is-uppercase">
                            Cloudwalk Digital Inc.
						</p>
						<p className="is-capital contextcolor">
                            The O Place 89 K-1st Street corner Judge Jimenez Kamuning Quezon City, Philippines
						</p>
					</Columns.Column>
					<Columns.Column id="contact-info-educ">
						<h1 className="contextcolortitle is-uppercase">
							Job Responsibilities
						</h1>
						<hr className="hr2" />
						<ul style={{listStyleType : "circle", marginLeft : "15px"}}>
                            <li className="is-capital contextcolor">
                                Demonstrate knowledge of Web Technologies such as, but not limited to, Laravel, HTML, CSS, Javascript and ReactJS
							</li>
                            <li className="is-capital contextcolor">
                                Accomplishing task/project in a fast-paced and deadline-driven environment
							</li>
                            <li className="is-capital contextcolor">
                                 Responsible for implementing finished products based on design requirements provided
							</li>
                            <li className="is-capital contextcolor">
                                Integrate data from various databases and back-end services
							</li>
							<li className="is-capital contextcolor">
                                Work alongside other engineers on the team to elevate technology and consistently apply best practices.
							</li>
							<li className="is-capital contextcolor">
                                Ability to research and execute on solutions for programming challenges and willingness to commit to results, and working on minor bugs fixes.
							</li>
							<li className="is-capital contextcolor">
                                Collaborate cross-functionally with business users, project managers and other engineers to achieve elegant solutions.
							</li>
                            <li className="is-capital contextcolor">
                                Analyze and develop software development methodology in an agile environment.
							</li>

						</ul>
					</Columns.Column>
					<Columns>
						<Columns.Column size={6}>
							<span className="contextcolort">Follow Me :</span>
							<span id="about-span">
								<a
										href="https://www.facebook.com/jlretzel07"
										target="_blank"
									>
										<i
											class="fab fa-facebook-square fa-lg"
											id="about-icon"
										></i>
									</a>
								</span>
								<span id="about-span">
									<a
										href="https://www.linkedin.com/in/john-lester-sabandal-331b52105/"
										target="_blank"
									>
									<i
										class="fab fa-linkedin fa-lg"
										id="about-icon"
									></i>
									</a>
							</span>
						</Columns.Column>
						<Columns.Column size={6} className="has-text-right">
							<Button
								className="btn-next is-uppercase"
								onClick={workingExperienceClickHandler}
							>
								back
							</Button>

							<Button
								className="btn-next is-uppercase"
								onClick={workingExperienceWilconClickHandler}
							>
								Next
							</Button>
						</Columns.Column>
					</Columns>
					</Fade>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default WorkingExperiencecwd;
