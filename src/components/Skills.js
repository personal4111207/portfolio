import React from "react";
import {
	Container,
	Heading,
	Columns,
	Card,
	Content,
	Progress,
	Button
} from "react-bulma-components";
import Fade from 'react-reveal/Fade';

const Skills = props => {
	// console.log(props);

	const zuitBootcampClickHandler = () => {
		props.zuittBootcamp();
	};


	return (
		<Container className="has-text-centered margin"> 
			<Columns>
				<Columns.Column id="contact-form">
					<h1>
						<span className="span-gold">Design & Code</span>
					</h1>
					<h1 id="title" className="is-uppercase">my skills</h1>
					<hr className="hr" />

					<Columns>
						<Columns.Column size={6}>
						<Fade big>
							 <div className="progress-container has-text-right">
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">html</h2>
							 		<span className="text-progress">85%</span>
							     	 <Progress max={100} value={85} color="warning" size="small" />
							 	</div>
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">CSS</h2>
							 		<span className="text-progress">80%</span>
							      	<Progress max={100} value={80} color="warning" size="small" />
							 	</div>
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">bootstrap</h2>
							 		<span className="text-progress">85%</span>
							      	<Progress max={100} value={85} color="warning" size="small" />
							 	</div>
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">javascript</h2>
							 		<span className="text-progress">75%</span>
							      	<Progress max={100} value={75} color="warning" size="small" />
							 	</div>

							</div>
							</Fade>
						</Columns.Column>

						<Columns.Column size={6}>
						<Fade big>
							 <div className="progress-container has-text-right">
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">php</h2>
							 		<span className="text-progress">80%</span>
							      	<Progress max={100} value={80} color="warning" size="small" />
							 	</div>
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">laravel</h2>
							 		<span className="text-progress">80%</span>
							      	<Progress max={100} value={80} color="warning" size="small" />
							 	</div>
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">react js</h2>
							 		<span className="text-progress">79%</span>
							      	<Progress max={100} value={79} color="warning" size="small" />
							 	</div>
							 	<div className="progress-content">
							 		<h2 className="has-text-left text-progress is-uppercase">web development</h2>
							 		<span className="text-progress">80%</span>
							      	<Progress max={100} value={80} color="warning" size="small" />
							 	</div>

							</div>
					</Fade>
						</Columns.Column>
					</Columns>
					<Columns.Column className="has-text-right">
						<Button
							className="btn-next is-uppercase"
							onClick ={zuitBootcampClickHandler}
						
						>
						Previous
						</Button>						
					</Columns.Column>


				</Columns.Column>

			</Columns>
		</Container>

		);
}

export default Skills;